const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const jwt_decode = require("jwt-decode");
const bodyParser = require('body-parser');

app.use(bodyParser.json());


io.on('connection', (socket) => {
    if (!socket.handshake.query) {
        return;
    }
    const user = jwt_decode(socket.handshake.query["accessToken"]);
    console.log(user.sub);
    socket.join(user.sub);
});

http.listen(3000, () => {
    console.log("listening on port 3000");
});

app.post('/submit', (req, res) => {
    if (!Array.isArray(req.body.followers)) {
        console.log("no friends submitted");
        return res.status(400).send();
    }
    req.body.followers.forEach(follower => {
        io.in(follower).emit("new-crap", req.body.crap);
    });
    return res.status(200).send();
});
